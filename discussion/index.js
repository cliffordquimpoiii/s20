//[SECTION] Introduction to JSON (JavaScript Object Notation)

//json -> is a 'Data Representation Format' similar to XML and YAML

//Uses of JSON data format:

		//=> Commonly used for API and Configuration.
		//=> JSON is also used in serealizing different data types into 'bytes'.

		//What is Serealizing?
			//==> is the process of converting data into a series of bytes for easier transimission/transfer of information.


			//a 'byte' => is a unit of data that is eight binary digits (1 and 0) that is used to represent a character(letters, numbers, typographic symbols)


			//Benefits => once a piece of data has been serealized they become 'lightweight', it becomes a lot easier to tranfer or transmit over a network or connection. 


			//[SECTION] Structure of JSON format

				//=> JSON data is similar to the structure of a JS Object.

				//=> JS Objects are NOT to be confused with JSON.
				//SYNTAX: 

					{
						propertyA: valueA, 
						propertyB: valueB
					}

					//JSON uses double quotes ()"" for its property names.

					//example:
					{
						"city": "Quezon City",
						"province": "Metro Manila",
						"country": "Philippines"
					}